<?php

namespace App\Http\Controllers\Twitter;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Model\TimeLine;
use App\Model\Users;
use Validator;

class HomeController extends Controller
{
    public function index()
    {
        $data['title'] = "Home";
        $data['status'] = Timeline::orderby('id','ASC')->get();
        return view('twitter.home', $data);
    }

    public function statussave(Request $req)
    {
        $post = $req->all();

        $timeline = new TimeLine;
        $timeline->id_user = Session::get('id');
        $timeline->status = $post['status'];
        $timeline->save();

        return redirect()->route('home')->with('status','Status Update');
    }

    public function profile($id)
    {
        $data['title'] = "Profile";
        $user = Users::find($id);

        if($user->profile == NULL){
            $data['imageurl'] = 'profile.png';
        }else{
            $data['imageurl'] = $user->profile;
        }

        $data['user'] = $user;

        return view('twitter.profile',$data);
    }

    public function profilesave(Request $req)
    {
        $post = $req->all();
        $user = Users::find($post['id']);

        $validator = Validator::make($req->all(), [
            'name' => 'required',
            'email' => 'required|unique:users,email,'.$user->id
        ]);

        if($validator->fails()){
            return redirect()->route('profile',$post['id'])->withErrors($validator)->withInput();
        }

        
        $user->email = $post['email'];
        $user->name = $post['name'];

        if(@$post['profile'] != ''){
            $gambar       = $post['profile'];
            $gambarname   = $gambar->getClientOriginalName();
            $gambar->move(\base_path() ."/public/images", @$gambarname);
        }

        if(@$gambarname != ''){
            $user->profile   = $gambarname;
        }

        if(@$post['password'] != ''){
            $user->password = bcrypt($post['password']);
        }

        $user->save();
        return redirect()->route('profile', $post['id'])->with('status','Data has been updated!');
    }
}
