<?php

namespace App\Http\Controllers\Twitter;

use Validator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use App\Model\Users;

class LoginController extends Controller
{
    public function form()
    {
        $data['title'] = 'Login Page';
        return view('twitter.login', $data);
    }

    public function doregister(Request $req)
    {
        $post = $req->all();

        $newuser = new Users;

        $validator = Validator::make($req->all(), [
            'password' => 'required',
            'name' => 'required',
            'email' => 'required|unique:users,email'
        ]);

        if($validator->fails()){
            return redirect()->route('login')->withErrors($validator)->withInput();
        }

        $newuser->name = $post['name'];
        $newuser->email = $post['email'];
        $newuser->password = bcrypt($post['password']);
        $newuser->save();
        //dd($newuser);
        
        return redirect()->route('login')->with('status', 'Data has been created');
    }

    public function dologin(Request $req)
    {
        $post = $req->all();

        $user = Users::where('email',$post['email'])->first();

        $this->validate($req,[
            'email' => 'required',
            'password' => 'required'
        ]);

        if($user != NULL){

        	if(Hash::check($post['password'],$user->password)){
        		Session::put('id',$user->id);
                Session::put('name',$user->name);
                Session::put('email',$user->email);
                Session::put('loginStatus',TRUE);

                
                return redirect()->route('home')->with('status','Has been login');
        	} 

        }
        return redirect()->route('login')->with('wrongstatus','Wrong username and password');

    }

    public function dologout()
    {
        Session::forget('id');
        Session::forget('name');
        Session::forget('nama_user');
        Session::forget('email');
        Session::forget('loginStatus');

        return redirect()->route('login')->with('status','Anda Sudah Log Out!');
    }
}
