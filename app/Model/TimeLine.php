<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TimeLine extends Model
{
    protected $table = 'timeline';

    public $timestamps = false;

    public function setCreatedAtAttribute($value) { 
        $this->attributes['created_at'] = \Carbon\Carbon::now(); 
    }

    public function user()
    {
        return $this->belongsTo('App\Model\Users','id_user','id');
    }
}
