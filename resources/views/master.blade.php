<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ $title }}</title>
    <link rel="stylesheet" href="{{ url('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ url('css/style.css') }}">
    <link rel="stylesheet" href="{{ url('scss/app.css') }}">
</head>
<body>
<header class="masthead">
    <div class="container h-100">
        <div class="row h-100 align-items-center">
            <div class="col-12 text-center">
                <p class="lead">Twitter Application</p>
            </div>
        </div>
    </div>
</header>
<section>
    @yield('content')
</section>    
<script src="{{ url('js/jquery-3.4.1.min.js') }}"></script>
@yield('javascript')
</body>
</html>