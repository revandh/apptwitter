@extends('master')
@section('content')
<div class="col-md-12 col-xs-12">
    <div class="login">
        @if(session()->has('wrongstatus'))
        <br/>      
        <section class="content-header" style="min-height:0px !important">
            <div class="row">
                <div class="col-xs-12 col-md-12">
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-ban"></i> Notification!</h4>
                        {{ session()->get('wrongstatus') }}
                    </div>
                </div>
            </div>
        </section>
        @endif
        @if(session()->has('status'))
        <br/>      
        <section class="content-header" style="min-height:0px !important">
            <div class="row">
                <div class="col-xs-12 col-md-12">
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-ban"></i> Notification!</h4>
                        {{ session()->get('status') }}
                    </div>
                </div>
            </div>
        </section>
        @endif
        @if($errors->any())
        <br/>      
        <section class="content-header" style="min-height:0px !important">
            <div class="row">
                <div class="col-xs-12 col-md-12">
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-ban"></i> Notification!</h4>
                        {{$errors->first()}}
                    </div>
                </div>
            </div>
        </section>
        @endif
        <h2>Login</h2>
        <form method="POST" action="{{ route('dologin') }}" id="login">
            {{ csrf_field() }}
            <div class="form-group">
                <input type="email" name="email" class="form-control" id="email" placeholder="Email" required>
            </div>
            <div class="form-group">
                <input type="password" name="password" id="password" class="form-control" placeholder="Password" required>
            </div>
            <div class="form-group text-center">
                <button type="submit" class="btn btn-primary">Login</button>
            </div>
        </form>
    </div>

    <hr>

    

    <div class="register">
        <h2>Register</h2>
        <form method="POST" action="{{ route('doregister') }}">
            {{ csrf_field() }}
            <div class="form-group">
                <input type="email" name="email" class="form-control" placeholder="Email" required>
            </div>
            <div class="form-group">
                <input type="text" name="name" class="form-control" placeholder="Name" required>
            </div>
            <div class="form-group">
                <input type="password" name="password" class="form-control" placeholder="Password" required>
            </div>
            <div class="form-group text-center">
                <button type="submit" class="btn btn-primary">Register</button>
            </div>
        </form>
    </div>
</div>
@endsection
@section('javascripts')
@endsection