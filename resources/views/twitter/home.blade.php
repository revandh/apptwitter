@extends('master')
@section('content')
<div class="col-md-12 col-xs-12">
  <div class="content-status">
      <form action="{{ route('statussave') }}" method="post">
          {{ csrf_field() }}
        <div class="input-group">
          <input type="text" name="status" class="form-control" placeholder="Update Status" required>
          <span class="input-group-btn">
              <button class="btn btn-primary" type="submit">Go!</button>
          </span>
        </div>
      </form>
  </div>

  <div class="content-body">
    @foreach ($status as $item)
    @if ($item->id_user != Session::get('id'))
    <div class="container-body">      
      <img src="{{ url('images/',($item->user->profile == NULL?"profile.png":$item->user->profile)) }}" alt="Avatar" style="width:100%;">
      <p>{{ $item->status }}</p>
    </div>
    @else
    <div class="container-body darker-green">
      <a href="{{ route('profile', ['id' => Session::get('id')]) }}">
        <img src="{{ url('images/',($item->user->profile == NULL?"profile.png":$item->user->profile)) }}" alt="Avatar" class="right" style="width:100%;">
      </a>      
      <p>{{ $item->status }}</p>
    </div>
    @endif
    @endforeach
    
  </div>
</div>
@endsection