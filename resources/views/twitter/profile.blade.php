@extends('master')
@section('content')
<div class="col-md-12 col-xs-12">
  <div class="content-body">
      @if(session()->has('status'))
      <br/>      
      <section class="content-header" style="min-height:0px !important">
          <div class="row">
              <div class="col-xs-12 col-md-12">
                  <div class="alert alert-success alert-dismissible">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                      <h4><i class="icon fa fa-ban"></i> Notification!</h4>
                      {{ session()->get('status') }}
                  </div>
              </div>
          </div>
      </section>
      @endif
      @if($errors->any())
      <br/>      
      <section class="content-header" style="min-height:0px !important">
          <div class="row">
              <div class="col-xs-12 col-md-12">
                  <div class="alert alert-danger alert-dismissible">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                      <h4><i class="icon fa fa-ban"></i> Notification!</h4>
                      {{$errors->first()}}
                  </div>
              </div>
          </div>
      </section>
      @endif
      <div class="header-title">
        <h2>Profile</h2>
        <a href="{{ route('dologout') }}" class="logout btn btn-danger float-right">Logout</a>
      </div>
      <form method="POST" action="{{ route('profilesave') }}" enctype="multipart/form-data" id="profile">
          {{ csrf_field() }}
          <input type="hidden" name="id" value="{{ $user->id }}">
          <div class="form-group">
              <input type="email" value="{{ $user->email }}" name="email" class="form-control" id="email" placeholder="Email" required>
          </div>
          <div class="form-group">
              <input type="text" value="{{ $user->name }}" name="name" id="name" class="form-control" placeholder="Name" required>
          </div>
          <div class="form-group">
              <input type="password" name="password" id="password" class="form-control" placeholder="Password">
              <?= (@$user->id) ? '<p style="color:red;" class="help-block pull-right"><small>Dont fill if you dont change the password</small></p>' : ''?>
          </div>
          <div class="form-group">
              <input style="display:none" name="profile" id="input-image-hidden" onchange="document.getElementById('image-preview').src = window.URL.createObjectURL(this.files[0])" type="file" accept="image/jpeg, image/png">
              <img id="image-preview" class="image-preview" src="{{ url('images/',$imageurl) }}">
              <a class="upload btn btn-success" onclick="HandleBrowseClick('input-image-hidden');" >Upload Profile</a>
          </div>
          <div class="form-group text-center">
              <button type="submit" class="btn btn-primary">Update</button>
              <a href="{{ route('home') }}" class="btn btn-secondary">Cancel</a>
          </div>
      </form>
  </div>
</div>
@endsection
@section('javascript')
<script type="text/javascript">
  function HandleBrowseClick(input_image)
  {
      var fileinput = document.getElementById(input_image);
      fileinput.click();
  }     
</script>
@endsection