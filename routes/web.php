<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => '/', 'namespace' => 'Twitter'] , function(){
    Route::get('/', 'LoginController@form')->name('login');
    Route::post('/doregister','LoginController@doregister')->name('doregister');
    Route::post('/dologin','LoginController@dologin')->name('dologin');
    Route::get('/dologout','LoginController@dologout')->name('dologout');

    Route::group(['prefix' => '/home','middleware' => 'login'], function () {
        Route::get('/', 'HomeController@index')->name('home');
        Route::post('/statussave','HomeController@statussave')->name('statussave');
        Route::get('/profile/{id}','HomeController@profile')->name('profile');
        Route::post('/profilesave','HomeController@profilesave')->name('profilesave');
    });
});
